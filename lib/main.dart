import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue,
          title: const Text(
            "First Screen of My API",
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    color: Colors.red[500],
                    width: MediaQuery.sizeOf(context).width * 0.15,
                    height: MediaQuery.sizeOf(context).width * 0.15,
                    child: const Center(child: Text("1")),
                  ),
                  Container(
                    color: Colors.yellow,
                    width: MediaQuery.sizeOf(context).width * 0.20,
                    height: MediaQuery.sizeOf(context).width * 0.20,
                    child: const Center(child: Text("2")),
                  ),
                  Container(
                    color: Colors.green,
                    width: MediaQuery.sizeOf(context).width * 0.30,
                    height: MediaQuery.sizeOf(context).width * 0.30,
                    child: const Center(
                      child: Text("3"),
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        color: Colors.red[500],
                        width: MediaQuery.sizeOf(context).width * 0.15,
                        height: MediaQuery.sizeOf(context).width * 0.15,
                        child: const Center(child: Text("1")),
                      ),
                      Container(
                        color: Colors.yellow,
                        width: MediaQuery.sizeOf(context).width * 0.20,
                        height: MediaQuery.sizeOf(context).width * 0.20,
                        child: const Center(child: Text("2")),
                      ),
                      Container(
                        color: Colors.green,
                        width: MediaQuery.sizeOf(context).width * 0.30,
                        height: MediaQuery.sizeOf(context).width * 0.30,
                        child: const Center(
                          child: Text("3"),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Column(
                    children: [
                      Container(
                        color: Colors.red[500],
                        width: MediaQuery.sizeOf(context).width * 0.15,
                        height: MediaQuery.sizeOf(context).width * 0.15,
                        child: const Center(child: Text("1")),
                      ),
                      Container(
                        color: Colors.yellow,
                        width: MediaQuery.sizeOf(context).width * 0.20,
                        height: MediaQuery.sizeOf(context).width * 0.20,
                        child: const Center(child: Text("2")),
                      ),
                      Container(
                        color: Colors.green,
                        width: MediaQuery.sizeOf(context).width * 0.30,
                        height: MediaQuery.sizeOf(context).width * 0.30,
                        child: const Center(child: Text("3")),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
